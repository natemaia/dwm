# dwm mini

A simplified and stripped down fork of dwm with and some quality of life improvements.

---

[dwm](https://dwm.suckless.org/) is an extremely fast, small, and dynamic window manager for X.


## Requirements

In order to build dwm you need the Xlib header files.

For the default keybindings you need to install [st](https://st.suckless.org/) and [dmenu](https://bitbucket.org/natemaia/dmenu/) or edit `config.h`

## Installation

Edit the `config.mk` to suit your system (by default installed to `/usr`) then run `make clean install` *(as root if needed)*

## Running dwm

To simply launch dwm, add the following to `~/.xinitrc`
```
exec dwm
```

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.
```
DISPLAY=foo.bar:1 exec dwm
```
This will start dwm on display :1 of the host foo.bar.


## Configuration

Configuration is done by creating/editing `config.h` and (re)compiling the source code.

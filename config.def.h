/* See LICENSE file for copyright and license details. */

/* appearance */
static const int borderpx = 1;
static const int numws = 10;
static const char *focused = "steel blue";
static const char *unfocused = "black";

/* layout */
static const int nmaster = 1;     /* number of clients in master area */
static const float mfact = 0.55;  /* factor of master area size [0.05..0.95] */
static const Layout layouts[] = { { tile }, { monocle }, { NULL }, };

/* rules */
static const Rule rules[] = {
	/* class             instance  workspace  isfloating  monitor */
	{ "Firefox",          NULL,      0,          0,          -1 },
	{ "Gimp",             NULL,      0,          1,          -1 },
	{ "Steam",            NULL,      0,          1,          -1 },
	{ "Pavucontrol",      NULL,      0,          1,          -1 },
	{ "Transmission-gtk", NULL,      0,          1,          -1 },
};

/* key definitions */
#define MODKEY Mod1Mask
#define WSKEY(KEY, WS) { KeyPress, MODKEY,             KEY, view,   {.ui = WS} }, \
                       { KeyPress, MODKEY|ShiftMask,   KEY, send,   {.ui = WS} }, \
                       { KeyPress, MODKEY|ControlMask, KEY, follow, {.ui = WS} }

/* manipulated in spawn(), for setting program monitor like dmenu -m */
static char currentmon[2] = "0";

static Key keys[] = {
	/* type       modifier          key         function     argument */
	{ KeyPress,   MODKEY|ShiftMask, XK_q,       quit,        {0} },
	{ KeyPress,   MODKEY|ShiftMask, XK_r,       reset,       {0} },
	{ KeyPress,   MODKEY|ShiftMask, XK_space,   togglefloat, {0} },
	{ KeyPress,   MODKEY,           XK_q,       killclient,  {0} },
	{ KeyPress,   MODKEY,           XK_Tab,     zoom,        {0} },
	{ KeyPress,   MODKEY,           XK_j,       focusstack,  {.i = +1 } },
	{ KeyPress,   MODKEY,           XK_k,       focusstack,  {.i = -1 } },
	{ KeyPress,   MODKEY,           XK_i,       incnmaster,  {.i = +1 } },
	{ KeyPress,   MODKEY,           XK_d,       incnmaster,  {.i = -1 } },
	{ KeyPress,   MODKEY,           XK_h,       setmfact,    {.f = -0.01} },
	{ KeyPress,   MODKEY,           XK_l,       setmfact,    {.f = +0.01} },
	{ KeyPress,   MODKEY,           XK_t,       setlayout,   {.v = &layouts[0]} },
	{ KeyPress,   MODKEY,           XK_m,       setlayout,   {.v = &layouts[1]} },
	{ KeyPress,   MODKEY,           XK_f,       setlayout,   {.v = &layouts[2]} },
	{ KeyPress,   MODKEY|ShiftMask, XK_Return,  spawn,       {.v = (char *[]){"st", NULL}} },
	{ KeyPress,   MODKEY,           XK_p,       spawn,       {.v = (char *[]){"dmenu_run", "-m", currentmon, NULL}} },
	{ KeyRelease, MODKEY,           XK_Print,   spawn,       {.v = (char *[]){"scrot", NULL}} },
	{ KeyPress,   MODKEY|ShiftMask, XK_Print,   spawn,       {.v = (char *[]){"scrot", "-s", NULL}} },
	{ KeyPress,   0,                0x1008ff12, spawn,       {.v = (char *[]){"pamixer", "-t", NULL}} },
	{ KeyPress,   0,                0x1008ff13, spawn,       {.v = (char *[]){"pamixer", "-i", "2", NULL}} },
	{ KeyPress,   0,                0x1008ff11, spawn,       {.v = (char *[]){"pamixer", "-d", "2", NULL}} },
	WSKEY(XK_1, 1), WSKEY(XK_2, 2), WSKEY(XK_3, 3), WSKEY(XK_4, 4), WSKEY(XK_5, 5),
	WSKEY(XK_6, 6), WSKEY(XK_7, 7), WSKEY(XK_8, 8), WSKEY(XK_9, 9), WSKEY(XK_0, 10),
#ifdef XINERAMA
	{ KeyPress,   MODKEY,           XK_comma,   focusmon,    {.i = -1 } },
	{ KeyPress,   MODKEY,           XK_period,  focusmon,    {.i = +1 } },
	{ KeyPress,   MODKEY|ShiftMask, XK_comma,   sendtomon,   {.i = -1 } },
	{ KeyPress,   MODKEY|ShiftMask, XK_period,  sendtomon,   {.i = +1 } },
#endif
};
